s1 = sum([n**2 for n in range(10**6)]) #creates a list then calculates
print(s1)
s2 = sum((n**2 for n in range(10**6))) #uses a generator
print(s2)
s3 = sum(n**2 for n in range(10**6)) #uses a generator, removes redunant parenteses.
print(s3)