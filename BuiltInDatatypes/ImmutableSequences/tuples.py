#A tuple is a squence of arbitrary Python objects, items sperated by comma
#Often used to crate patterns and hold variables or any other object
def aTup(t):
    print(t)


t = () #empty tuble
aTup(type(t))

one_element_tuple = (42,) #need the comma!!
three_element_tuple = (1, 3, 5)
a, b, c = 1, 2, 3 #tuple for multiple assignment
aTup((a, b, c))

aTup(3 in  three_element_tuple) #membership test

#my_tuple = 1, 2, 3 same as my_tuple = (1, 2, 3)

#pythonic way of doing this:

#one line swapping

a, b = b, a
aTup((a, b))
