#4 ways to make strings
def aStr(s):
    print(s)


str1 = 'This is a string. We built it with single quotes.'
str2 = "This is also a string, but built with double quotes"
str3 = '''
    This is built using triple quotes
    ...so that it can span multiple lines
'''

str4 = """
    Same thing with double quotes
"""

#the differences between single and double quotes in python should be investigated

aStr(len(str1)) #lenth of string
