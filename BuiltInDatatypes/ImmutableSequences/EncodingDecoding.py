def aStr(s):
    print(s)

s = "你好， 我的名字是伯瑞恩"
aStr(type(s)) #prints type class 'str'

encode_s = s.encode('utf-8')

aStr(encode_s) #prints the byte encoding of s
aStr(type(encode_s)) #prings type from encode_s "class 'byte'"

decode_s = encode_s.decode('utf-8')
aStr(decode_s) #prints s

