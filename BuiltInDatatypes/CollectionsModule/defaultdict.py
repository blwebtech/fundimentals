######## NORMAL DICT  #########
d = {}
d['age'] = d.get('age', 0) + 1
print(d)
e = {'age': 39}
e['age'] = e.get('age', 0) + 1
print(e)


####### default dict ########
from collections import defaultdict
dd = defaultdict(int)  #int is the default type (0 the value)
dd['age'] += 1
print(dd)
dd['age'] = 39
dd['age'] += 1
print(dd)