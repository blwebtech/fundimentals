from fractions import Fraction

def aNum(n):
    print(n)


aNum(Fraction(10, 6)) #It's a bank in West Michigan?

aNum(Fraction(1, 3) + Fraction(2, 3)) #evaulates to 1

f = Fraction(10, 6)
aNum(f.numerator)
aNum(f.denominator)

#As mentioned in the previoius section.. Python defaults to a float
#Which has a 64 bit display digit limition. Decminal is what you'd use for
#higher precision, which is to say, most math operation should be decorated as decimial

from decimal import Decimal as D

aNum(D(3.14))
aNum(D('0.1') * D(3) - D('0.3'))

