def filter_ints(v):
    v = [num for num in v if num != 0]
    return [num for num in v if is_postive(num)]

def is_postive(num):
    return num > 0