from unittest import TestCase, result
from unittest.mock import patch, call
from nose.tools import assert_equal, assert_list_equal
import sys
sys.path.insert(0, "/home/ryan/dev/sandbox/fundimentals/TestingProfilingExceptions/Testing")
#from filter_funcs import filter_ints
from filter_funcs_refactored import filter_ints

class FilterIntsTestCase(TestCase):
    @patch('filter_funcs.is_positive')
    def test_filter_ints(self, is_postive_mock):
        # preparation
        v = [3, -4, 0, 5, 8]


        #execute
        filter_ints(v)


        #verification
        assert_equal(
            [call(3), call(-4), call(0), call(5), call(8)],
            is_postive_mock.call_args_list
        )

    def test_filter_ints_return_value(self):
        v = [3, -4, 0, -2, 5, 0, 8, -1]

        result = filter_ints(v)

        assert_list_equal([3, 5, 8], result)
