from unittest import TestCase
from nose.tools import assert_equal
import sys
sys.path.insert(0, "/home/ryan/dev/sandbox/fundimentals/TestingProfilingExceptions/Testing")
from filter_funcs import is_positive

class FilterIntsTestCase(TestCase):
    def test_is_positive(self):
        assert_equal(False, is_positive(-1))
        assert_equal(False, is_positive(0))
        assert_equal(True, is_positive(1))