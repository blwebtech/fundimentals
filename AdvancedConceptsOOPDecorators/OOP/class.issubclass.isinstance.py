#Is-A or Has-A
class Engine():
    def start(self):
        pass

    def stop(self):
        pass

class ElectricEngine(Engine):
    pass

class V8Engine(Engine):
    pass

class Car():
    engine_cls = Engine

    def __init__(self):
        self.engine = self.engine_cls() #has an engine

    def start(self):
        name = self.engine.__class__.__name__
        style = self.__class__.__name__
        print(
            f'Starting engine {name} for car {style}. Wroom..wroom'
        )

        self.engine.start()

    def stop(self):
        self.engine.stop()

class RaceCar(Car): #is a car
    engine_cls = V8Engine

class CityCar(Car): #is a car
    engine_cls = ElectricEngine

class F1Car(RaceCar): #Is a racecar and also Is-a car
    engine_cls = V8Engine

car = Car()
racecar = RaceCar()
citycar = CityCar()
f1car = F1Car()
# cars = [car, racecar, citycar, f1car]


cars = [(car, 'car'), (racecar, 'racecar'), (f1car, 'f1car')]
car_classes = [Car, RaceCar, F1Car]

for car, car_name in cars:
    for class_  in car_classes:
        belongs = isinstance(car, class_)
        msg = ' is a' if belongs else ' is not'
        print(car_name, msg, class_.__name__)

for class1 in car_classes:
    for class2 in car_classes:
        is_subclass = issubclass(class1, class2)
        name1 = class1.__name__
        name2 = class2.__name__
        msg = f'{"is" if is_subclass else "is not"} a subclass of'
        print(class1.__name__, msg, class2.__name__)
