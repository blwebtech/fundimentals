class A:
    def __init__(self, factor):
        self.__factor = factor

    def op1(self):
        fe = self.__factor
        print(f'Op1 with factor {fe}')

class B(A):
    def op2(self, factor):
        fe = self.__factor = factor
        print(f'Op2 with factor {self.__factor}')



obj = B(100)
obj.op1()
obj.op2(42)
obj.op1()

print(obj.__dict__.keys())
