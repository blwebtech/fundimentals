class Squre():
    side = 8
    def area(self): #self is a reference to an instance
        return self.side ** 2

sq = Squre()
print(sq.area())
print(Squre.area(sq))

sq.side = 10
print(sq.area())