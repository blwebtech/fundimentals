def func(**kwargs):
    print(kwargs)

# All calls equivalent.  They print: {'a':1,'b':2 }
func(a=1, b=42)
func(**{'a': 1, 'b': 2 })
func(**dict(a=1, b=42))

