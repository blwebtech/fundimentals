x = [1, 2, 3]
def func(x):
    x[1] = 42 #list objects are mutable
    x = 'someting else'

func(x)
print(x)
