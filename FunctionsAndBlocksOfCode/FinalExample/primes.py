from math import sqrt, ceil

def get_primes(n):
    """Calculate a list of primes up to n (included)"""
    primeList = []
    for candidate in range(2, n +1):
        is_prime = True
        root = int(ceil(sqrt(candidate))) #division limit
        for prime in primeList: #we try only the primes
            if prime > root:
                break
            if candidate % prime == 0:
                is_prime = False
                break
        if is_prime:
            primeList.append(candidate)
    return primeList


print(len(get_primes(50000000)))
