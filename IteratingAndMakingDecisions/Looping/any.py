items = [1, None, 0.0, True, 0, 7]#careful 1 evaluates to true in python
found = False
for item in items:
    print('scanning item', item)
    if item:
        found = True
        break

if found:
    print('At least one item evaluated to True')
else:
    print('All items evaluate to False')