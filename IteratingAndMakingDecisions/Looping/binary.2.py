n = 42
remainders = []
while n > 0:
    n, remainder = divmod(n, 2)
    remainders.append(remainder)

remainders = remainders[::-1]
print(''.join(str(x) for x in remainders))