primes = [] #This will contain the primes in the end

upto = 100

for n in range(2, upto + 1):
    is_prime = True
    for devisor in range(2, n):
        if n % devisor == 0:
            is_prime = False
            break
    else:
        primes.append(n)

print(primes)