#local vs global

def local():
    #m doesn't exist in this scope, nor was it passed in.
    print(m, 'printing from the local scope')

m = 5
print(m, 'printing from the global scope')

local()